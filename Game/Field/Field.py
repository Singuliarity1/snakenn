import pygame as pg

class Field:
    def __init__(self, width: int, height: int):
        self.__width = width
        self.__height = height
        self.__root = pg.display.set_mode((self.__width, self.__height))
        self.__surf = pg.Surface((self.__width, self.__height))

    def show(self):
        self.__surf.fill((255, 255, 255))
        self.__root.blit(self.__surf,(self.__width, self.__height))
        self.__root.blit(self.__surf, (0, 0))
        pg.display.update()

    def getSurface(self):
        return self.__surf

    def setSurface(self, surface):
        self.__surf = surface
        self.__root.blit(self.__surf, (0, 0))
        pg.display.update()



