import random
import pygame as pg

from Game.Snake.Snake import Snake


class SnakeFood:
    isEated: bool = True
    xPos: int = 0
    yPos: int = 0

    def __init__(self, snake: Snake):
        self.__snake = snake

    def getFoodPosition(self) -> list:
        return [self.xPos, self.yPos]

    def checkFeedSnake(self):
        if self.getFoodPosition()[0] - 5 <= self.__snake.getHeadPosition()[0] <= self.getFoodPosition()[0] + 5 \
                and self.getFoodPosition()[1] - 5 <= self.__snake.getHeadPosition()[1] <= self.getFoodPosition()[1] + 5:
            self.isEated = True
            self.__snake.changeLength()

    def makeFoodOnField(self, width: int, height: int) -> list:
        xPos = random.randint(0, width)
        yPos = random.randint(0, height)
        return [xPos, yPos]

    def addFood(self, surface):
        if self.isEated:
            self.xPos, self.yPos = self.makeFoodOnField(600, 600)
            self.isEated = False
        food = pg.Surface((5, 5))
        food.fill((0, 255, 0))
        surface.blit(food, (self.xPos, self.yPos))
        self.checkFeedSnake()
        return surface
