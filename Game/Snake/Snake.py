import array
import pygame as pg


class Snake:
    snakeBodyRadius = 15

    def __init__(self, x: int, y: int):
        self.__snakeBody: dict = dict(
            position=[[x, y]],
            vector=[[1, 0]]
        )
        self.__snakeLength: int = 1
        self.__width: int = 0
        self.__height: int = 0
        self.__isLose: bool = False

    def getMianVector(self):
        return self.__snakeBody['vector'][0]

    def setFieldSize(self, width: int, height: int):
        self.__width = width
        self.__height = height

    def isLose(self):
        return self.__isLose

    def changeLength(self):
        self.__snakeBody['position'].insert(0, self.__snakeBody["position"].__getitem__(0))
        self.__snakeBody['vector'].insert(0, self.__snakeBody["vector"].__getitem__(0))
        self.__snakeLength += 1

    def getHeadPosition(self) -> list:
        return self.__snakeBody['position'][0]
    def checkLoose(self) -> bool:
        headPosition = self.__snakeBody['position'][0]
        if headPosition[0] <= self.snakeBodyRadius or headPosition[0] >= 600 - self.snakeBodyRadius:
            return True
        if headPosition[1] <= self.snakeBodyRadius or headPosition[1] >= 600 - self.snakeBodyRadius:
            return True
        if self.__snakeLength > 4:
            for item in range(4,self.__snakeLength):
                if self.__snakeBody['position'][item] == headPosition:
                    return True
        return False
    def updatePosition(self):
        for item in range(self.__snakeLength-1, -1, -1):
            if item == 0:
                self.__snakeBody['position'][item] = [
                    self.__snakeBody['position'][item][0] + self.__snakeBody['vector'][item][0] * self.snakeBodyRadius,
                    self.__snakeBody['position'][item][1] + self.__snakeBody['vector'][item][1] * self.snakeBodyRadius,
                ]
            else:
                self.__snakeBody['position'][item] = [
                    self.__snakeBody['position'][item-1][0],
                    self.__snakeBody['position'][item-1][1],
                ]
            if 0 < item:
                self.__snakeBody['vector'][item] = self.__snakeBody['vector'][item - 1]

    def paintSnake(self, surface):
        for item in range(self.__snakeLength):
            snake = pg.Surface((self.snakeBodyRadius, self.snakeBodyRadius))
            snake.fill((250, 0, 250))
            xPos = self.__snakeBody['position'][item][0]
            yPos = self.__snakeBody['position'][item][1]
            surface.blit(snake, (xPos, yPos))
        return surface

    def changeVectorMove(self, vector: array):
        self.__snakeBody['vector'][0] = vector
