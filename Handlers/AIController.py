
class AIController:

    def pushUp(self) -> list:
        return [0, -1]
    def pushDown(self) -> list:
        return [0, 1]

    def pushLeft(self) -> list:
        return [-1, 0]

    def pushRight(self) -> list:
        return [1, 0]