import collections
import matplotlib.pyplot as plt
import numpy as np

import tensorflow as tf

from keras import layers

vector = {
    'top':1,
    'down':2,
    'left':3,
    'right':4
}

model = tf.keras.Sequential()
model.add(layers.Embedding(input_dim=3600,output_dim=4))
model.add(layers.LSTM(128))
model.add(layers.Dense(10))
model.summary()