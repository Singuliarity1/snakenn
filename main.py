import sys

from Game.Field.Field import Field
from Game.Snake.Snake import Snake
import pygame as pg

from Game.Snake.SnakeFood import SnakeFood
pg.init()
snake = Snake(250, 250)
field = Field(600, 600)
snakeFood = SnakeFood(snake)
field.show()
surface = field.getSurface()
surface = snake.paintSnake(surface)
field.setSurface(surface)
clock = pg.time.Clock()

while 1:
    for i in pg.event.get():
        if i.type == pg.QUIT:
            sys.exit()
        if i.type == pg.KEYDOWN:
            if i.key == pg.K_UP and snake.getMianVector()[1] != 1:
                snake.changeVectorMove([0, -1])
            if i.key == pg.K_DOWN and snake.getMianVector()[1] != -1:
                snake.changeVectorMove([0, 1])
            if i.key == pg.K_LEFT and snake.getMianVector()[0] != 1:
                snake.changeVectorMove([-1, 0])
            if i.key == pg.K_RIGHT and snake.getMianVector()[0] != -1:
                snake.changeVectorMove([1, 0])
    if snake.checkLoose():
        f1 = pg.font.Font('./Fonts/font.ttf', 36)
        text1 = f1.render('Loose', True, (180, 0, 0))
        surface.blit(text1, (10, 50))
    else:
        surface.fill((255, 255, 255))
        snake.updatePosition()
        surface = snake.paintSnake(surface)
        snakeFood.addFood(surface)
    field.setSurface(surface)
    clock.tick(20)
